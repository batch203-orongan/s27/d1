/*
	What is DATA Model?

		- A data model describes how data is organized and group in database.

		- By creating data models we can anticipate which data will be managed by the Database Management System in accordance to the application to be Develop.

	Data Modelling

		- Database should have a purpose and its organization must be related to the kind of application we are building.

	Scenarion:
		A course booking system appliction where a user can enroll into a course.

	Type: Course Booking System (Web App)
	Description: A course booking system appliction where a user can enroll into a course.

	Features: 
		- User Login (User Authentication)
		- User Registration

		Costumer/Authenticated User:
			- View Courses (all active courses)
			- Enroll Course 

		Admin Users:
			- Add Course
			- Update Course
			- Archive/Unarchive a course (soft delete/reactivate the course)
			- View Course (All Courses active/inactive)
			- View/Manage User Account

		All Users (guest, costumers, admin)
			-View Active Courses

*/

/*

	Data Models
		- Blueprints for our documents that we can follow and structure our data.
		- Show the relationship/s between our data.
	
*/

	User {
		id - unique identifier for the document, // system generated.
		username - string
		firstName - string,
		lastName - string,
		email - string,
		password - string,
		mobileNumber - sting,
		isAdmin - boolean, // no need to fill out by the costumer, and it will be set to "false"
	}

	Course {
		id - unique identifier for the document, // system generated.
		name - string,
		description - string,
		price - number,
		slots - number,
		schedule - dateTime/string,
		istructor - string,
		isActive - boolean // true - the course is currently active and offered.
	}

	Enrollments {
		id - unique identifier for the document, // system generated.
		UserId - the unique identifier for the user (from the user document).
		username - string (optional),
		courseId - the unique identifier for the course( from ht Course document),
		courseName - string (optional),
		isPaid - boolean,
		dateEnrolled - dateTime,
	}

/*

	MongoDB Data Modeling Design

	There are two ways in creating a MongoDb Model:
		=> Embedded Data Models
			- generally known as "denormalize" models, and takes advantages of MongoDB's rich documents.
			- subdocument embedded in a parent document.

			example:
				Parent document {
					subdocument {
						
					}
				}

		=> Reference Data Models
			- This are known as "normalize" data models and describe the relationdhip using the reference between documents.
			- Use document ID to connect a document to another document.	

*/

/*
	Model Relationships

	To be able to properly organize an application database we should also be able to identify the relationships between our models.
*/
	// One to One - This relationship means that a model is exclusively related to only one model.

	Employee:
	{
		"id": "2022Dev",
		"firstName": "Jack",
		"lastName": "Sullivan",
		"email": "jsdev2021@gmail.com"
	}

	Credentials:
	{
		"id": "creds_01",
		"employee_id": "2022Dev",
		"role": "developer",
		"team": "tech"
	}
/*
	In MongoDB, one to one relationship can be expressed in another way instead of referencing.

		Embedding - embed/put another document in a document.
		Subdocuments - are documents embedded in a parent document
*/
	Employee
		{
			"id": "2022Dev",
			"firstName": "Jack",
			"lastName": "Sullivan",
			"email": "jsdev2021@gmail.com",
			"credentials": 	{
				"id": "creds_01",
				"role": "developer",
				"team": "tech"
			}
		}
/*
	One to Many

		One model is related to multiple other models.
		However the other models are only related to one.

		Person - many email address

		Many Email Address - one person

		One Blog post - many comments

			A blog post can have multiple comments but each comment should only refer to single blog post.
*/
		Blog : {

			"id": "blog1-22",
			"title": "This is an Awesome Blog!",
			"content": "This is an awesome blog that I created!",
			"createdOn": "7/26/2022",
			"author": "blogwriter1"

		}

		Comments: 
		{
			"id":"blogcomment1",
			"comment": "Awesome Blog!",
			"author": "blogwriter1",
			"blog_id": "blog1-22"
		}
		{
			"id":"blogcomment2",
			"comment": "Meh. Not awesome at all.",
			"author": "notHater22",
			"blog_id": "blog1-22"
		}
/*
		In MongoDB, one to many relationship can also be expressed in another way:

			Subdocument Array - an array of subdocuments per single parent document
*/
		Blog : {

			"id": "blog1-22",
			"title": "This is an Awesome Blog!",
			"content": "This is an awesome blog that I created!",
			"createdOn": "7/26/2022",
			"author": "blogwriter1",
			"comments": [

				{
					"id":"blogcomment1",
					"comment": "Awesome Blog!",
					"author": "blogwriter1"
				},
				{
					"id":"blogcomment2",
					"comment": "Meh. Not awesome at all.",
					"author": "notHater22"
				}
			]
		}

/*
		Many to Many

			Multiple documents are related to multiple documents.

			users - courses

			user can enroll many courses
			course can have many enrolees. (user)

			When a many to many relationship is created, for models to relate to each other, an associative entity is created. Associative entity is a model that relates models in the many to many relationship

			user - enrollment - course

			So that a user can relate to a course, so that we can track the enrollment of a user to a course, we have to create the details for their enrollment.
*/
			// With Referencing

			user {

				id - unique identifier for the document,
				username,
				firstName,
				lastName,
				email,
				password,
				mobileNumber,
				isAdmin

			}

			course {

				id - unique for the document
				name,
				description,
				price,
				slots,
				schedule,
				instructor,
				isActive

			}

			enrollment {

				id - document identifier,
				userId - the unique identifier for the user,
				courseId - the unique identifier for the course,
				isPaid,
				dateEnrolled

			}
/*
	In MongoDB, many to many relationship can also be expressed in an another way:

		Two Way Embedding - In two way embedding, the associative entity is created and embedded in both models/documents
*/
		user {

			id - unique identifier for the document,
			username,
			firstName,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin,
			enrollments: [
				{

					id - document identifier,
					courseId - the unique identifier for the course,
					courseName - optional,
					isPaid,
					dateEnrolled
				}
			]

		}


		course {

			id - unique for the document
			name,
			description,
			price,
			slots,
			schedule,
			instructor,
			isActive,
			enrollees: [

				{
					id - document identifier,
					userId,
					userName(optional),
					isPaid,
					dateEnrolled
				}

			]

		}
